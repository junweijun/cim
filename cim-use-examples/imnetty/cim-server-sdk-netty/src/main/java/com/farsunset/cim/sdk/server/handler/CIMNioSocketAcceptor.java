/*
 * Copyright 2013-2019 Xia Jun(3979434@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***************************************************************************************
 *                                                                                     *
 *                        Website : http://www.farsunset.com                           *
 *                                                                                     *
 ***************************************************************************************
 */
package com.farsunset.cim.sdk.server.handler;

import com.farsunset.cim.sdk.server.coder.AppMessageDecoder;
import com.farsunset.cim.sdk.server.coder.AppMessageEncoder;
import com.farsunset.cim.sdk.server.coder.WebMessageDecoder;
import com.farsunset.cim.sdk.server.coder.WebMessageEncoder;
import com.farsunset.cim.sdk.server.constant.CIMConstant;
import com.farsunset.cim.sdk.server.model.CIMSession;
import com.farsunset.cim.sdk.server.model.HeartbeatRequest;
import com.farsunset.cim.sdk.server.model.HeartbeatResponse;
import com.farsunset.cim.sdk.server.model.SentBody;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class CIMNioSocketAcceptor{
	private static final Logger LOGGER = LoggerFactory.getLogger(CIMNioSocketAcceptor.class);

	private final HashMap<String, CIMRequestHandler> innerHandlerMap = new HashMap<>();
    private final ConcurrentHashMap<String,Channel> channelGroup = new ConcurrentHashMap<>();


	private EventLoopGroup appBossGroup;
	private EventLoopGroup appWorkerGroup;

	private EventLoopGroup webBossGroup;
	private EventLoopGroup webWorkerGroup;

	private final Integer appPort;
	private final Integer webPort;
	private final CIMRequestHandler outerRequestHandler;
	private final ChannelHandler channelEventHandler = new FinalChannelEventHandler();

	/**
	 *  读空闲时间(秒)
	 */
	public static final int READ_IDLE_TIME = 150;

	/**
	 *  写接空闲时间(秒)
	 */
	public static final int WRITE_IDLE_TIME = 120;

	/**
	 * 心跳响应 超时为30秒
	 */
	public static final int PONG_TIME_OUT = 10;

	private CIMNioSocketAcceptor(Builder builder){
		this.webPort = builder.webPort;
		this.appPort = builder.appPort;
		this.outerRequestHandler = builder.outerRequestHandler;
	}

	public void bind() {
 
		innerHandlerMap.put(CIMConstant.CLIENT_HEARTBEAT, new HeartbeatHandler());

		if (appPort != null){
			bindAppPort();
		}

		if (webPort != null){
			bindWebPort();
		}
	}

	public void destroy(EventLoopGroup bossGroup , EventLoopGroup workerGroup) {
		if(bossGroup != null && !bossGroup.isShuttingDown() && !bossGroup.isShutdown() ) {
			try {bossGroup.shutdownGracefully();}catch(Exception ignore) {}
		}

		if(workerGroup != null && !workerGroup.isShuttingDown() && !workerGroup.isShutdown() ) {
			try {workerGroup.shutdownGracefully();}catch(Exception ignore) {}
		}
	}


	/**
	 * 关闭长连接服务
	 */
    public void destroy() {
    	this.destroy(appBossGroup,appWorkerGroup);
		this.destroy(webBossGroup,webWorkerGroup);
	}


	public Channel getManagedSession(String id) {
		if (id == null) {
			return null;
		}
		return channelGroup.get(id);
	}

	private void bindAppPort(){
		appBossGroup = new NioEventLoopGroup();
		appWorkerGroup = new NioEventLoopGroup();
		ServerBootstrap bootstrap = createServerBootstrap(appBossGroup,appWorkerGroup);
		bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
			@Override
			public void initChannel(SocketChannel ch){
				ch.pipeline().addLast(new AppMessageDecoder());
				ch.pipeline().addLast(new AppMessageEncoder());
				ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
				ch.pipeline().addLast(new IdleStateHandler(READ_IDLE_TIME, WRITE_IDLE_TIME, 0));
				ch.pipeline().addLast(channelEventHandler);
			}
		});

		ChannelFuture channelFuture = bootstrap.bind(appPort).syncUninterruptibly();
		channelFuture.channel().newSucceededFuture().addListener(future -> {
			String logBanner = "\n\n" +
					"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" +
					"*                                                                                   *\n" +
					"*                                                                                   *\n" +
					"*                   App Socket Server started on port {}.                        *\n" +
					"*                                                                                   *\n" +
					"*                                                                                   *\n" +
					"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n";
			LOGGER.info(logBanner, appPort);
		});
		channelFuture.channel().closeFuture().addListener(future -> this.destroy(appBossGroup,appWorkerGroup));
	}

/*
HttpServerCodec：将请求和应答消息解码为HTTP消息
   HttpServerCodec是netty针对http编解码的处理类，但是这些只能处理像http get的请求,也就是数据带在url问号后面的http请求
HttpRequestDecoder
   这个是最核心的方法，包括了解析请求行，请求头，请求体，但是会将请求行和请求头整合起来形成一个请求DefaultHttpRequest传递到后面，
 因为请求体可能很大，所以也可能会有多次封装，那后面处理器就可能收到多次消息体。如果是GET的话是没有消息体的，首先收到一个DefaultHttpRequest，
 然后是一个空的LastHttpContent。如果是POST的话，先收到DefaultHttpRequest，然后可能多个内容DefaultHttpContent和一个DefaultLastHttpContent。
HttpObjectAggregator：将HTTP消息的多个部分合成一条完整的HTTP消息
   前面我们讲了， 一个HTTP请求最少也会在HttpRequestDecoder里分成两次往后传递，
  第一次是消息行和消息头，第二次是消息体，哪怕没有消息体，也会传一个空消息体。
  如果发送的消息体比较大的话，可能还会分成好几个消息体来处理，往后传递多次，
  这样使得我们后续的处理器可能要写多个逻辑判断，比较麻烦，那能不能把消息都整合成一个完整的，
  再往后传递呢，当然可以，用HttpObjectAggregator。其实就是用来解析post情求的
ChunkedWriteHandler：向客户端发送HTML5文件
*/

	private void bindWebPort(){
		webBossGroup = new NioEventLoopGroup();
		webWorkerGroup = new NioEventLoopGroup();
		ServerBootstrap bootstrap = createServerBootstrap(webBossGroup,webWorkerGroup);
		bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

			@Override
			public void initChannel(SocketChannel ch){
				ch.pipeline().addLast(new HttpServerCodec());
				ch.pipeline().addLast(new ChunkedWriteHandler());
				ch.pipeline().addLast(new HttpObjectAggregator(65536));
				ch.pipeline().addLast(new WebMessageEncoder());
				ch.pipeline().addLast(new WebMessageDecoder());
				ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
				ch.pipeline().addLast(channelEventHandler);
			}

		});

		ChannelFuture channelFuture = bootstrap.bind(webPort).syncUninterruptibly();
		channelFuture.channel().newSucceededFuture().addListener(future -> {
			String logBanner = "\n\n" +
					"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" +
					"*                                                                                   *\n" +
					"*                                                                                   *\n" +
					"*                   Websocket Server started on port {}.                         *\n" +
					"*                                                                                   *\n" +
					"*                                                                                   *\n" +
					"* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n";
			LOGGER.info(logBanner, webPort);
		});
		channelFuture.channel().closeFuture().addListener(future -> this.destroy(webBossGroup,webWorkerGroup));
	}

	private ServerBootstrap createServerBootstrap(EventLoopGroup bossGroup,EventLoopGroup workerGroup){
		ServerBootstrap bootstrap = new ServerBootstrap();
		bootstrap.group(bossGroup, workerGroup);
		bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
		bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
		bootstrap.channel(NioServerSocketChannel.class);
		return bootstrap;
	}

	@Sharable
	private class FinalChannelEventHandler extends  SimpleChannelInboundHandler<Object>{
		@Override
		protected void channelRead0(ChannelHandlerContext ctx, Object data) {

			if (data instanceof HeartbeatResponse){
				return;
			}

			SentBody body = (SentBody) data;

			CIMSession session = new CIMSession(ctx.channel());

			CIMRequestHandler handler = innerHandlerMap.get(body.getKey());
			/*
			 * 如果有内置的特殊handler需要处理，则使用内置的
			 */
			if (handler != null) {
				handler.process(session, body);
				return;
			}

			/*
			 * 有业务层去处理其他的sentBody
			 */
			outerRequestHandler.process(session, body);
		}
		//channelInactive 被触发一定是和服务器断开
		@Override
		public void channelInactive(ChannelHandlerContext ctx) {
			channelGroup.remove(ctx.channel().id().asShortText());

			CIMSession session = new CIMSession(ctx.channel());
			SentBody body = new SentBody();
			body.setKey(CIMConstant.CLIENT_CONNECT_CLOSED);
			outerRequestHandler.process(session, body);

		}

		@Override
		public void channelActive(ChannelHandlerContext ctx){
			channelGroup.put(ctx.channel().id().asShortText(),ctx.channel());
		}
/*
 	https://blog.csdn.net/linuu/article/details/51509847
	1）客户端连接服务端
	2）在客户端的的ChannelPipeline中加入一个比较特殊的IdleStateHandler，设置一下客户端的写空闲时间，例如5s
	3）当客户端的所有ChannelHandler中4s内没有write事件，则会触发userEventTriggered方法（上文介绍过）
	4）我们在客户端的userEventTriggered中对应的触发事件下发送一个心跳包给服务端，检测服务端是否还存活，防止服务端已经宕机，客户端还不知道
	5）同样，服务端要对心跳包做出响应，其实给客户端最好的回复就是“不回复”，这样可以服务端的压力，假如有10w个空闲Idle的连接，那么服务端光发送心跳回复，则也是费事的事情，那么怎么才能告诉客户端它还活着呢，其实很简单，因为5s服务端都会收到来自客户端的心跳信息，那么如果10秒内收不到，服务端可以认为客户端挂了，可以close链路
	6）加入服务端因为什么因素导致宕机的话，就会关闭所有的链路链接，所以作为客户端要做的事情就是短线重连
	*/
		@Override
		public void userEventTriggered(ChannelHandlerContext ctx, Object evt){

			if (! (evt instanceof IdleStateEvent)){
				return;
			}

			IdleStateEvent idleEvent = (IdleStateEvent) evt;

			if (idleEvent.state().equals(IdleState.WRITER_IDLE)) {
				ctx.channel().attr(AttributeKey.valueOf(CIMConstant.HEARTBEAT_KEY)).set(System.currentTimeMillis());
				ctx.channel().writeAndFlush(HeartbeatRequest.getInstance());
			}

			/*
			 * 如果心跳请求发出30秒内没收到响应，则关闭连接
			 */
			if (idleEvent.state().equals(IdleState.READER_IDLE)) {

				Long lastTime = (Long) ctx.channel().attr(AttributeKey.valueOf(CIMConstant.HEARTBEAT_KEY)).get();
				if (lastTime != null && System.currentTimeMillis() - lastTime >= PONG_TIME_OUT) {
					ctx.channel().close();
				}

				ctx.channel().attr(AttributeKey.valueOf(CIMConstant.HEARTBEAT_KEY)).set(null);
			}
		}

	}


	public static class Builder{

		private Integer appPort;
		private Integer webPort;
		private CIMRequestHandler outerRequestHandler;

		public Builder setAppPort(Integer appPort) {
			this.appPort = appPort;
			return this;
		}

		public Builder setWebsocketPort(Integer port) {
			this.webPort = port;
			return this;
		}

		/**
		 * 设置应用层的sentBody处理handler
		 */
		public Builder setOuterRequestHandler(CIMRequestHandler outerRequestHandler) {
			this.outerRequestHandler = outerRequestHandler;
			return this;
		}
		//将当前config设置到buidler中去
		public CIMNioSocketAcceptor build(){
			return new CIMNioSocketAcceptor(this);
		}

	}

}
