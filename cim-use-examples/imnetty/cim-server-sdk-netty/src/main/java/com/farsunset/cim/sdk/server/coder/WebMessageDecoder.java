package com.farsunset.cim.sdk.server.coder;

import com.farsunset.cim.sdk.server.model.SentBody;
import com.farsunset.cim.sdk.server.model.proto.SentBodyProto;
import com.google.protobuf.InvalidProtocolBufferException;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.websocketx.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.ConcurrentHashMap;
//https://www.cnblogs.com/carl10086/p/6188808.html
public class WebMessageDecoder extends SimpleChannelInboundHandler<Object> {

    private final static String URI = "ws://localhost:%d";

    private static final ConcurrentHashMap<String, WebSocketServerHandshaker> handShakerMap = new ConcurrentHashMap<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(WebMessageDecoder.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws InvalidProtocolBufferException {
        // 传统的HTTP接入,但是走的是websocket
        if (msg instanceof FullHttpRequest) {
            handleHandshakeRequest(ctx, (FullHttpRequest) msg);
        }
        // WebSocket接入 ,处理websocket客户端的消息
        if (msg instanceof WebSocketFrame) {
            handlerWebSocketFrame(ctx, (WebSocketFrame) msg);
        }
    }

    private void handleHandshakeRequest(ChannelHandlerContext ctx, FullHttpRequest req) {
       /* 通过WebSocketServerHandshakerFactory创建socket对象并通过handshaker握手创建连接
                在连接创建好后的所以消息流动都是以WebSocketFrame来体现*/
        int port = ((InetSocketAddress)ctx.channel().localAddress()).getPort();
        // 构造握手响应返回 ,创建一个握手处理器
        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(String.format(URI,port), null, false);
       //从请求头获取WEBSOCKET版本，根据不同版本，返回不同握手对象
        WebSocketServerHandshaker handShaker = wsFactory.newHandshaker(req);

        handShakerMap.put(ctx.channel().id().asLongText(), handShaker);
        // 通过它构造握手响应消息返回给客户端，
        // 同时将WebSocket相关的编码和解码类动态添加到ChannelPipeline中，用于WebSocket消息的编解码，
        // 添加WebSocketEncoder和WebSocketDecoder之后，服务端就可以自动对WebSocket消息进行编解码了
        //https://blog.csdn.net/wangwei19871103/article/details/104651753
        //握手对象进行握手，其实就是发送响应数据。
        // 先会创建一个FullHttpResponse 响应，然后把跟HTTP相关的聚合，压缩处理器删除，
        // 如果有HttpServerCodec，那就在前面添加websocket的编解码器，
        // 等发送响应成功了把HttpServerCodec删了。如果是HTTP编解码器，就把解码器先替换成websocket的解码器，
        // 等发送响应成功了，再把编码器替换成websocket的编码器。
        handShaker.handshake(ctx.channel(), req);
    }
  /*
    BinaryWebSocketFrame 包含了二进制数据
    TextWebSocketFrame 包含了文本数据
    ContinuationWebSocketFrame 包含属于上一个BinaryWebSocketFrame或TextWebSocketFrame的文本数据或者二进制数据
    CloseWebSocketFrame 表示一个CLOSE请求，包含一个关闭的状态码和关闭的原因
    PingWebSocketFrame请求传输一个PongWebSocketFrame
    PongWebSocketFrame 作为一个对于PingWebSocketFrame的响应被发
   */


    private void handlerWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame) throws InvalidProtocolBufferException {
        // 判断是否是关闭链路的指令
        if (frame instanceof CloseWebSocketFrame) {
            handlerCloseWebSocketFrame(ctx, (CloseWebSocketFrame) frame);
            return;
        }
        //判断是否是Ping消息
        if (frame instanceof PingWebSocketFrame) {
            handlerPingWebSocketFrame(ctx, (PingWebSocketFrame) frame);
            return;
        }

        handlerBinaryWebSocketFrame(ctx, (BinaryWebSocketFrame) frame);
    }

    private void handlerCloseWebSocketFrame(ChannelHandlerContext ctx, CloseWebSocketFrame frame){
        WebSocketServerHandshaker handShaker = handShakerMap.get(ctx.channel().id().asLongText());
        handShaker.close(ctx.channel(), frame.retain());
    }

    private void handlerPingWebSocketFrame(ChannelHandlerContext ctx, PingWebSocketFrame frame){
        ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
    }

    private void handlerBinaryWebSocketFrame(ChannelHandlerContext ctx, BinaryWebSocketFrame frame) throws InvalidProtocolBufferException {

        byte[] data = new byte[frame.content().readableBytes()];
        frame.content().readBytes(data);

        SentBodyProto.Model bodyProto = SentBodyProto.Model.parseFrom(data);
        SentBody body = new SentBody();
        body.setKey(bodyProto.getKey());
        body.setTimestamp(bodyProto.getTimestamp());
        body.putAll(bodyProto.getDataMap());
        ctx.fireChannelRead(body);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause){
        ctx.close();
        LOGGER.warn("Exception caught",cause);
    }
}
